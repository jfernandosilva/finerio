package mx.finerio.test.exception;

public class LoginException extends Exception {
    public LoginException(String message) {
        super(message);
    }
}
