package mx.finerio.test.dao;

import mx.finerio.test.model.Movement;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovementDao extends MongoRepository<Movement, String> {
}
