package mx.finerio.test.controllers;

import lombok.extern.slf4j.Slf4j;
import mx.finerio.test.exception.LoginException;
import mx.finerio.test.model.AuthParams;
import mx.finerio.test.services.IProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;

@RestController
@Slf4j
public class TestController {

    @Autowired
    private IProcessorService processorService;

    @GetMapping("/runTest")
    public ResponseEntity runTest(@RequestHeader(value = "Authorization", required = true) String authorizationHeader) throws LoginException {
        AuthParams authParams;
        try {
            String authEncoded = authorizationHeader.substring(6);
            String authDecoded = new String(Base64.getDecoder().decode(authEncoded), StandardCharsets.UTF_8);
            authParams = AuthParams.builder()
                    .userName(authDecoded.split(":")[0])
                    .password(authDecoded.split(":")[1])
                    .build();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            log.error(ex.getMessage(), ex);
            return ResponseEntity.badRequest().body(Collections.singletonMap("mensaje", "error en el header Authorization"));
        }
        processorService.executeProcess(authParams);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler({LoginException.class})
    public ResponseEntity handleException(LoginException ex) {
        return ResponseEntity.internalServerError().body(Collections.singletonMap("mensaje", "error al hacer login en finero"));
    }
}
