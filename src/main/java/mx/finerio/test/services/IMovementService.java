package mx.finerio.test.services;

import mx.finerio.test.model.AuthenticationResponse;
import mx.finerio.test.model.MovementData;
import mx.finerio.test.model.Profile;

public interface IMovementService {
    MovementData getMovements(AuthenticationResponse authenticationResponse, Profile profile, Integer offset, Integer max);
}
