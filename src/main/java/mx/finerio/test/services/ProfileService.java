package mx.finerio.test.services;

import mx.finerio.test.model.AuthenticationResponse;
import mx.finerio.test.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProfileService implements IProfileService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Profile getUserProfile(AuthenticationResponse authenticationResponse) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authenticationResponse.getAccessToken());

        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        return restTemplate.exchange("https://api.finerio.mx/api/me", HttpMethod.GET, requestEntity, Profile.class).getBody();
    }
}
