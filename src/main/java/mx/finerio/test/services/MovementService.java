package mx.finerio.test.services;

import mx.finerio.test.model.AuthenticationResponse;
import mx.finerio.test.model.MovementData;
import mx.finerio.test.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MovementService implements IMovementService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public MovementData getMovements(AuthenticationResponse authenticationResponse, Profile profile, Integer offset, Integer max) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(authenticationResponse.getAccessToken());

        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        return restTemplate
                .exchange("https://api.finerio.mx/api/users/{userId}/movements?deep=false&offset={offset}&max={max}&includeCharges=true&includeDeposits=true&includeDuplicates=true",
                        HttpMethod.GET, requestEntity, MovementData.class, profile.getId(), offset, max).getBody();


    }
}
