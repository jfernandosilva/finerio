package mx.finerio.test.services;

import mx.finerio.test.exception.LoginException;
import mx.finerio.test.model.AuthParams;
import mx.finerio.test.model.AuthenticationResponse;

public interface ILoginService {
    AuthenticationResponse executeLogin(AuthParams authParams) throws LoginException;
}
