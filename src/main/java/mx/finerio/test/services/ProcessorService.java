package mx.finerio.test.services;

import lombok.extern.slf4j.Slf4j;
import mx.finerio.test.dao.MovementDao;
import mx.finerio.test.exception.LoginException;
import mx.finerio.test.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ProcessorService implements IProcessorService {
    @Autowired
    private ILoginService loginService;

    @Autowired
    private IProfileService profileService;

    @Autowired
    private IMovementService movementService;

    @Autowired
    private MovementDao movementDao;


    @Override
    public void executeProcess(AuthParams authParams) throws LoginException {
        AuthenticationResponse authenticationResponse = loginService.executeLogin(authParams);
        log.debug("authenticationResponse");
        log.debug(authenticationResponse.toString());

        Profile userProfile = profileService.getUserProfile(authenticationResponse);
        log.debug("userProfile");
        log.debug(userProfile.toString());

        MovementData movementData1 = movementService.getMovements(authenticationResponse, userProfile, 0, 10);
        log.debug("movementData1");
        log.debug(movementData1.toString());
        saveMovements(movementData1.getData());

        MovementData movementData2 = movementService.getMovements(authenticationResponse, userProfile, 10, 10);
        log.debug("movementData2");
        log.debug(movementData2.toString());
        saveMovements(movementData2.getData());
    }

    private void saveMovements(List<Movement> movements) {
        // guarda o actualiza la lista de movimientos
        movementDao.saveAll(movements);
    }
}
