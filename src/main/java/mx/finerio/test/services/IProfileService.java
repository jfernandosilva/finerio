package mx.finerio.test.services;

import mx.finerio.test.model.AuthenticationResponse;
import mx.finerio.test.model.Profile;

public interface IProfileService {
    Profile getUserProfile(AuthenticationResponse authenticationResponse);
}
