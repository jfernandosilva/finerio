package mx.finerio.test.services;

import lombok.extern.slf4j.Slf4j;
import mx.finerio.test.exception.LoginException;
import mx.finerio.test.model.AuthParams;
import mx.finerio.test.model.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
@Slf4j
public class LoginService implements ILoginService {
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public AuthenticationResponse executeLogin(AuthParams authParams) throws LoginException {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            HttpEntity<AuthParams> httpEntity = new HttpEntity<>(authParams, headers);
            return restTemplate.postForEntity("https://api.finerio.mx/api/login", httpEntity, AuthenticationResponse.class).getBody();
        } catch (HttpClientErrorException ex) {
            log.error(ex.getMessage(), ex);
            log.error(ex.getResponseBodyAsString());
            throw new LoginException("Error al ejecutar login");
        }
    }
}
