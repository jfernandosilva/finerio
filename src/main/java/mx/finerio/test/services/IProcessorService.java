package mx.finerio.test.services;

import mx.finerio.test.exception.LoginException;
import mx.finerio.test.model.AuthParams;

public interface IProcessorService {
    void executeProcess(AuthParams authParams) throws LoginException;

}
