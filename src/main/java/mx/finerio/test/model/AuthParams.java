package mx.finerio.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthParams {
    @JsonProperty("username")
    private String userName;
    private String password;
}
