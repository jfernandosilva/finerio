package mx.finerio.test.model;

import lombok.Data;

@Data
public class Profile {
    private String id;
    private String email;
    private String name;
}
