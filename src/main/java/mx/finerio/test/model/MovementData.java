package mx.finerio.test.model;

import lombok.Data;

import java.util.List;

@Data
public class MovementData {
    List<Movement> data;
}
