package mx.finerio.test.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@Document("movimientos")
public class Movement {
    @Id
    private String id;
    private BigDecimal amount;
    private BigDecimal balance;
    private String customDescription;
    private String description;
    private String type;
}
