package mx.finerio.test.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AuthenticationResponse {
    @JsonProperty("username")
    private String userName;

    @JsonProperty("access_token")
    private String accessToken;
}